package com.twuc.webApp.web;

import com.twuc.webApp.model.Answer;
import com.twuc.webApp.model.Game;
import com.twuc.webApp.model.Guess;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;


import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class GameControllerTest {

    @Autowired
    private TestRestTemplate template;

    @BeforeEach
    void setUp() {
        template.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }

    @Test
    void should_return_create_games_success_status_and_body_no_content() {
        ResponseEntity<String> stringResponseEntity = template.postForEntity("/api/games", null, String.class);
        assertEquals(HttpStatus.CREATED, stringResponseEntity.getStatusCode());
    }

    @Test
    void should_return_create_games_when_giving_game_id() {
        ResponseEntity<String> stringResponseEntity = template.postForEntity("/api/games/3", null, String.class);
        assertEquals(HttpStatus.CREATED, stringResponseEntity.getStatusCode());
        assertEquals("/api/games/3", stringResponseEntity.getHeaders().getFirst("Location"));
    }

    @Test
    void should_return_400_when_create_games_and_game_id_exist() {
        ResponseEntity<String> stringResponseEntity = template.postForEntity("/api/games/2", null, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, stringResponseEntity.getStatusCode());
    }

    @Test
    void should_return_game_information_and_success_code() {
        ResponseEntity<Game> forEntity = template.getForEntity("/api/games/2", Game.class);
        assertEquals(HttpStatus.OK, forEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, forEntity.getHeaders().getContentType());
        assertEquals("1234", Objects.requireNonNull(forEntity.getBody()).getAnswer());
    }

    @Test
    void should_return_404_code_when_game_id_not_found() {
        ResponseEntity<String> forEntity = template.getForEntity("/api/games/4", String.class);
        assertEquals(HttpStatus.NOT_FOUND, forEntity.getStatusCode());
    }

    @Test
    void should_return_400_code_when_guess_number_illegal() {
        ResponseEntity<Answer> exchange = template.exchange("/api/games/9", HttpMethod.PATCH, new HttpEntity<>(new Guess("125")), Answer.class);
        assertEquals(HttpStatus.BAD_REQUEST, exchange.getStatusCode());
    }

    @Test
    void should_return_400_when_guess_number_size() {
        ResponseEntity<Answer> exchange = template.exchange("/api/games/9", HttpMethod.PATCH, new HttpEntity<>(new Guess("12343")), Answer.class);
        assertEquals(HttpStatus.BAD_REQUEST, exchange.getStatusCode());
    }

    @Test
    void should_return_400_when_guess_contain_character() {
        ResponseEntity<Answer> exchange = template.exchange("/api/games/9", HttpMethod.PATCH, new HttpEntity<>(new Guess("123E")), Answer.class);
        assertEquals(HttpStatus.BAD_REQUEST, exchange.getStatusCode());
    }

    @Test
    void should_return_400_when_guess_contain_repeat_number() {
        ResponseEntity<Answer> exchange = template.exchange("/api/games/9", HttpMethod.PATCH, new HttpEntity<>(new Guess("1111")), Answer.class);
        assertEquals(HttpStatus.BAD_REQUEST, exchange.getStatusCode());

    }

    @Test
    void should_return_false_when_guess_number_is_not_correct() {
        ResponseEntity<Answer> exchange = template.exchange("/api/games/9", HttpMethod.PATCH, new HttpEntity<>(new Guess("1235")), Answer.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
        assertFalse(Objects.requireNonNull(exchange.getBody()).isCorrect());
        assertEquals("2A2B", exchange.getBody().getHint());
    }

    @Test
    void should_return_false_and_hint_when_guess_number_is_not_correct() {
        ResponseEntity<Answer> exchange = template.exchange("/api/games/9", HttpMethod.PATCH, new HttpEntity<>(new Guess("1362")), Answer.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
        assertFalse(Objects.requireNonNull(exchange.getBody()).isCorrect());
        assertEquals("1A2B", exchange.getBody().getHint());
    }


    @Test
    void should_return_true_when_guess_number_correct() {
        ResponseEntity<Answer> exchange = template.exchange("/api/games/9", HttpMethod.PATCH, new HttpEntity<>(new Guess("1253")), Answer.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
        assertTrue(Objects.requireNonNull(exchange.getBody()).isCorrect());
        assertEquals("4A0B", exchange.getBody().getHint());
    }
}
