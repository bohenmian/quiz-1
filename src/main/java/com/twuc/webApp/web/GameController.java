package com.twuc.webApp.web;

import com.twuc.webApp.exception.GameIdExistException;
import com.twuc.webApp.exception.GameIdNotFoundException;
import com.twuc.webApp.model.Guess;
import com.twuc.webApp.model.Game;
import com.twuc.webApp.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/games")
public class GameController {

    private GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("")
    public ResponseEntity CreateGames() {
        return ResponseEntity.status(HttpStatus.CREATED)
                .build();
    }

    @PostMapping("/{gameId}")
    public ResponseEntity CreateGameWithId(@PathVariable Integer gameId) {
        gameService.createGame(gameId);
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", "/api/games/" + gameId)
                .build();
    }

    @GetMapping("/{gameId}")
    public ResponseEntity<Game> getGameInformation(@PathVariable Integer gameId) {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(gameService.getGameInformation(gameId));
    }

    @PatchMapping("/{gameId}")
    public ResponseEntity guess(@PathVariable Integer gameId, @RequestBody @Valid Guess guess) {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(gameService.guess(gameId, guess));
    }

    @ExceptionHandler(GameIdNotFoundException.class)
    public ResponseEntity handleGameIdNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .build();
    }

    @ExceptionHandler({IllegalArgumentException.class, GameIdExistException.class})
    public ResponseEntity handleIllegalArgumentException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .build();
    }
}
