package com.twuc.webApp.model;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Guess {

    @Pattern(regexp = "(^[0-9]*)")
    @Size(max = 4, min = 4)
    private String answer;

    public Guess() {
    }

    public Guess(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }
}
