package com.twuc.webApp.model;

public class Answer {

    private String hint;
    private boolean correct;

    public Answer() {
    }

    public Answer(String hint, boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public Answer(String hint) {
        this(hint, false);
    }

    public String getHint() {
        return hint;
    }

    public boolean isCorrect() {
        return correct;
    }
}
