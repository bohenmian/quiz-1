package com.twuc.webApp.model;

public class Game {

    private Integer id;
    private String answer;

    public Game() {
    }

    public Game(Integer id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public Integer getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }
}
