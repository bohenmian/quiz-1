package com.twuc.webApp.utils;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class NumberUtils {

    private static Set<String> getRandomNumber() {
        Set<String> set = new HashSet<>();
        Random random = new Random();
        while (set.size() < 4) {
            set.add(String.valueOf(random.nextInt(10)));
        }
        return set;
    }

    public static String getAnswer() {
        Set<String> randomNumber = getRandomNumber();
        Iterator<String> iterator = randomNumber.iterator();
        StringBuilder answer = new StringBuilder("");
        while (iterator.hasNext()) {
            answer.append(iterator.next());
        }
        return String.valueOf(answer);
    }
}
