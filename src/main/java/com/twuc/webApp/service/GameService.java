package com.twuc.webApp.service;

import com.twuc.webApp.model.Answer;
import com.twuc.webApp.model.Game;
import com.twuc.webApp.model.Guess;

public interface GameService {

    Game getGameInformation(Integer id);

    void createGame(Integer gameId);

    Answer guess(Integer gameId, Guess guess);
}
