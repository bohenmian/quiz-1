package com.twuc.webApp.service.impl;

import com.twuc.webApp.exception.GameIdExistException;
import com.twuc.webApp.exception.GameIdNotFoundException;
import com.twuc.webApp.model.Answer;
import com.twuc.webApp.model.Game;
import com.twuc.webApp.model.Guess;
import com.twuc.webApp.service.GameService;
import com.twuc.webApp.utils.NumberUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Service
public class GameServiceImpl implements GameService {

    private static final Map<Integer, Game> GAME_MAP = new HashMap<>();

    static {
        // 内置，方便测试
        GAME_MAP.put(2, new Game(2, "1234"));
        GAME_MAP.put(9, new Game(9, "1253"));
    }


    @Override
    public Game getGameInformation(Integer gameId) {
        if (!GAME_MAP.containsKey(gameId)) {
            throw new GameIdNotFoundException();
        }
        return GAME_MAP.get(gameId);
    }

    @Override
    public void createGame(Integer gameId) {
        if (GAME_MAP.containsKey(gameId)) {
            throw new GameIdExistException();
        }
        GAME_MAP.put(gameId, new Game(gameId, NumberUtils.getAnswer()));
    }

    @Override
    public Answer guess(Integer gameId, Guess guess) {
        if (Arrays.stream(guess.getAnswer().split("")).distinct().count() != 4) {
            throw new IllegalArgumentException();
        }
        if (!GAME_MAP.containsKey(gameId)) {
            throw new GameIdNotFoundException();
        }
        return guessAnswer(guess.getAnswer(), GAME_MAP.get(gameId).getAnswer());
    }

    private Answer guessAnswer(String guessAnswer, String answer) {
        int countA = 0;
        int countB = 0;
        for (int j = 0; j < guessAnswer.length(); j++) {
            if (guessAnswer.charAt(j) == answer.charAt(j)) {
                countA++;
                continue;
            }
            for (int k = 0; k < answer.length(); k++) {
                if (guessAnswer.charAt(j) == answer.charAt(k)) {
                    countB++;
                    break;
                }
            }
        }
        return countA == 4 ? new Answer("4A0B", true) : new Answer(countA + "A" + countB + "B");
    }
}
