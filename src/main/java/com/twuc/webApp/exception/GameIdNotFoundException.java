package com.twuc.webApp.exception;

public class GameIdNotFoundException extends RuntimeException {


    public GameIdNotFoundException() {
        super();
    }

    public GameIdNotFoundException(String message) {
        super(message);
    }
}
