package com.twuc.webApp.exception;

public class GameIdExistException extends RuntimeException {

    public GameIdExistException() {
        super();
    }

    public GameIdExistException(String message) {
        super(message);
    }
}
